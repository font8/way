# way

`way` (synonym of path) has low-level arithmetic functions
for evaluating Bézier curves (generally cubic or quadratic).
Evaluating the curves determines for any given point whether
it is inside or outside the figure described by curve.

`B` describes Bézier curves.

`P` describes polynomials (in one unknown).

`Point` describes a 2D point.


`B` instances support Hit methods which effectively intersect a
ray with a curve and return a series of points in the order
along the ray.

`P` instances have a Solve method.

Generally these methods only work up to cubic (degree 3).


## Testing

    go test

will run some tests.
in 2022 most of the testing is a number of randomly generated
cases (about 1 million in the current code configuration).

Each case is tested to check that expected roots are found,
and that the found roots evaluated to 0.
Both of these are subject to, highly unsatisfactory,
thresholding.

Testing millions of cases (1 billion cases takes about 30 minutes)
with reasonable thresholds still finds
cases where the returned answer is what seems like
an unreasonable distance away in representation steps:

    cub_test.go:125: Not at Zero: {[22.64681726130231 -40.87300586006438 24.71031433127012 -5] [1.7420628662540238]} evaluates to -2.1316282072803006e-14 Steps used: 200

    cub_test.go:125: Not at Zero: {[-31.46600204448824 57.09042352753839 -34.59273759533777 7] [1.6561053707625388]} evaluates to -2.842170943040401e-14 Steps used: 785

# END
