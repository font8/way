package way

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"sort"
	"strconv"
	"strings"
	"time"
)

import (
	"testing"
)

// vary the seed, for funsies
func init() {
	rand.Seed(time.Now().UnixNano())
}

// Coefficients for a cubic polynomial, and its roots
type CubicData struct {
	coeff []float64
	root  []float64
}

// A cubic, a factor (a divisor actually), a quotient, a remainder
type FactorData struct {
	cubic     []float64 // length 4
	factor    []float64 // length 2
	quotient  []float64 // length 3
	remainder float64
}

// cases are given in the form:
// D,C,B,A,r...
// where the first 4 items specify the cubic polynomial
// from its 0-degree term up to its 3-degree term:
// Ax³ + Bx² + Cx + D
// The remaining items are the roots (from 1 to 3).
// Sources:
// - https://brilliant.org/wiki/factor-polynomials-cubics/
// - https://en.wikipedia.org/wiki/Polynomial
// Case 319 (D is 319.something) has two solutions,
// adjacent in representation, each evaluate to 0.0
// 24.580096839220012 and 24.580096839220015
var cases_s = `
1,-1.5,0,0.5,-2.0
-35,6,4,3,1.6666666666666667
-5,0,0,5,1.0
0,3,0,1,0
-2,-1.5,0.75,0.25,2
-9,-6,0,1,3
319.5412589098602,-13,147.48058103532009,-6,24.580096839220015
-67.58779924954469,-9,-22.529266416514893,-3,-7.509755472171632
2,-1,-2,1,-1,1,2
`

// cubics and their factors
// D,C,B,A,u,v,q0,q1,q2,r
// where D,C,B,A give the cubic (as above)
// u,v specify a factor of it: (u + vx)
// q0,q1,q2 gives the quotient, a quadratic q0 + q1x + q2x²
// r gives the remainder

var case_factors_s = `
1,-1.5,0,0.5,2,1,0.5,-1,0.5,0
-35,6,4,3,-1.6666666666666667,1,21,9,3,0
-5,0,0,5,-1,1,5,5,5,0
0,3,0,1,-0,1,3,0,1,0
-2,-1.5,0.75,0.25,-2,1,1,1.25,0.25,0
-9,-6,0,1,-3,1,3,3,1,0
`

func GetCubicCases() []CubicData {
	cases := []CubicData{}
	for _, fs := range CellSlice(cases_s) {
		c := CubicData{fs[:4], fs[4:]}
		cases = append(cases, c)
	}
	return cases
}

func GetFactorCases() []FactorData {
	cases := []FactorData{}
	for _, fs := range CellSlice(case_factors_s) {
		f := FactorData{
			fs[:4],
			fs[4:6],
			fs[6:9],
			fs[9],
		}
		cases = append(cases, f)
	}
	return cases
}

// convert a CSV string into a [][]float64
func CellSlice(x string) [][]float64 {
	cs := [][]float64{}
	// trim and split on newline
	rows := strings.Split(strings.TrimSpace(x), "\n")
	for _, row := range rows {
		cells := strings.Split(row, ",")
		fs := make([]float64, len(cells))
		for i := range cells {
			f, err := strconv.ParseFloat(cells[i], 64)
			if err != nil {
				log.Fatal(err)
			}
			fs[i] = f
		}
		cs = append(cs, fs)
	}
	return cs
}

// Generates about 1 million additional random tests.
// On drj's laptop on 2022-10-03 this takes about 1.8 s to run.
func GetCubicTestData() []CubicData {
	randoms := []CubicData{}
	N := 999999
	for i := 0; i < N; i++ {
		root := rand.NormFloat64() * 9.0
		lin := []float64{-root, 1}
		quad := RandQuad()
		cubic := PolyMul(lin, quad)
		randoms = append(randoms, CubicData{
			cubic, []float64{root}})
	}
	return append(GetCubicCases(), randoms...)
}

// Return a random quadratic, with, for now, 0 real solutions.
// This means that if we make a cubic by multiplying by a
// linear term, it will have just one solution.
func RandQuad() []float64 {
	for {
		quad := make([]float64, 3)
		for i := range quad {
			quad[i] = math.Round(rand.NormFloat64() * 9.0)
		}
		// quad[0] = rand.NormFloat64()
		discriminant := quad[1]*quad[1] -
			4.0*quad[2]*quad[0]
		if true || discriminant < 0 {
			return quad
		}
	}
}

// Multiply two polynomials (univariate in the same variable).
// Each polynomial is represented by a slice of its coefficients:
// a[i] is the coefficient for x**i.
func PolyMul(a, b []float64) []float64 {
	result := make([]float64, len(a)+len(b)-1)
	for i := range a {
		for j := range b {
			k := i + j
			result[k] += a[i] * b[j]
		}
	}
	return result

}

func TestCub(t *testing.T) {
	for _, v := range GetCubicTestData() {
		cub := P{v.coeff}
		xs, err := cub.Solve()
		if err != nil {
			t.Log(err)
			t.FailNow()
		}

		if len(xs) == 0 {
			t.Errorf("Cubic %v: 0 roots found", v)
			t.FailNow()
		}

		CheckCubic(t, v, cub, xs)
	}
}

func TestDiv(t *testing.T) {
	for _, v := range GetFactorCases() {
		cub := P{v.cubic}
		q, r := cub.Divide(v.factor[0], v.factor[1])
		if len(q) != 3 {
			t.Errorf("Cubic %v / %v did not yield quadratic: %v",
				cub, v.factor, q)
		}
		for i := 0; i <= 2; i++ {
			if q[i] != v.quotient[i] {
				t.Errorf("Cubic %v / %v should be %v; is %v",
					cub, v.factor, v.quotient, q)
			}
		}
		if r != v.remainder {
			t.Errorf("Cubic %v / %v should remainder %v; is %v",
				cub, v.factor, v.remainder, r)
		}
	}
}

func CheckCubic(t *testing.T, v CubicData, cub P, xs []float64) {
	// Check all returned results are roots
	for _, x := range xs {
		y := cub.Eval(x)
		if math.Abs(y) > 1e-9 {
			t.Errorf("Not at Zero: Cubic %v; result %v evaluates to %v",
				v, x, y)
		}
	}

	// Check that all documented roots are found.
	for _, r := range v.root {
		x := NearestResult(r, xs)
		// with this threshold set to 1e-10 we
		// get a few errors per million
		if math.Abs(1-x/r) > 1e-9 {
			t.Errorf("Root not found: %v result: %v", v, x)
		}
	}
}

// How many representation steps is x away from the crossing point?
func CrossingSteps(cub P, x float64) (int, error) {
	y := cub.Eval(x)
	if y == 0 {
		return 0, nil
	}

	// Select the adjacent representations
	// and widen the interval one step at a time
	// until a crossing is found.
	p := x
	n := x
	yp := 0.0
	yn := 0.0
	AttemptLimit := 20000
	i := 1
	for ; i <= AttemptLimit; i++ {
		p = math.Nextafter(p, math.Inf(-1))
		n = math.Nextafter(n, math.Inf(1))
		yp = cub.Eval(p)
		yn = cub.Eval(n)
		if yp == 0 || yn == 0 {
			// one of the points is at exactly 0
			return i, nil
		}
		if math.Copysign(y, yp) != y {
			// crosses 0 just before x
			return i, nil
		}
		if math.Copysign(y, yn) != y {
			// crosses 0 just after x
			return i, nil
		}
	}
	return -1, fmt.Errorf("Too many steps to find crossing (> %v steps)", AttemptLimit)
}

// Return the x in xs that is nearest to r
func NearestResult(r float64, xs []float64) float64 {
	s := append([]float64(nil), xs...)
	sort.Slice(s, func(i, j int) bool {
		return math.Abs(s[i]-r) <
			math.Abs(s[j]-r)
	})
	return s[0]
}
