package way

import (
	"fmt"
	"log"
	"math"
)

// While the interface is degree independent,
// the solver only works for cubics.

// A polynomial is represented by its coefficients
// in a slice ([]float64);
// the coefficient for degree i is at index i
// (it is a univariate polynomial).

// To represent a cubic, supply 4 coefficients:
// Represents c[0] + c[1]x + c[2]x² + c[3]x³
// (trad)      D      C       B       A
type P struct {
	c []float64
}

func (p *P) Eval(x float64) float64 {
	a := p.c[len(p.c)-1]
	for i := len(p.c) - 2; i >= 0; i -= 1 {
		// Horner's method
		a = math.FMA(a, x, p.c[i])
	}
	return a
}

func (c *P) Solve() ([]float64, error) {
	if len(c.c) != 4 {
		return nil, fmt.Errorf("Sorry, this code only solves cubics %v", c)
	}
	rs := []float64{}
	quadratic := []float64(nil)
	if c.c[3] != 0 {
		aroot, err := c.SolveSingle()
		if err != nil {
			return nil, err
		}
		rs = append(rs, aroot)
		// Factor out the known root, reduce to quadratic
		factor := []float64{-aroot, 1}
		quadratic, _ = c.Divide(factor[0], factor[1])
	} else {
		// The "cubic" is actually a quadratic
		quadratic = c.c[:3]
	}
	qroots, err := QuadraticSolve(quadratic)
	if err != nil {
		return rs, err
	}
	return append(rs, qroots...), nil
}

// Divide the cubic by (ax + b),
// yielding a quadratic quotient and a remainder.
func (c *P) Divide(b, a float64) ([]float64, float64) {
	if len(c.c) != 4 {
		log.Fatal("Divide should only be applied to a cubic", c)
	}
	// q for quotient, but also quadratic.
	q := make([]float64, 3)
	// Product terms: P = (ax+b)(q[2]x**2 + q[1]x + q[0]) + r
	// x**2 coefficient of quotient
	q[2] = c.c[3] / a
	// b*q[2] + a*q[1] == B
	q[1] = (c.c[2] - b*q[2]) / a
	// b*q[1] + a*q[0] == C
	q[0] = (c.c[1] - b*q[1]) / a
	// b*q[0] + r == D
	r := c.c[0] - b*q[0]
	return q, r
}

// Following https://www.johndcook.com/blog/2022/04/05/cubic/
// Return a single root.
func (p *P) SolveSingle() (float64, error) {
	if len(p.c) != 4 {
		log.Fatal("SolveSingle should only be applied to a cubic", p)
	}
	if p.c[3] == 0 {
		return -999.9, fmt.Errorf("Cubic %v has cubic term =0", *p)
	}
	if p.c[0] == 0 {
		// easy case, 0 is a root, and x is a factor.
		return 0, nil
	}

	// Transform to a polynomial that has
	// exactly one solution between 0 and 1.
	// For detailed notes see Cook article above.

	// Make monic (A is 1), by
	// dividing though by A.
	A := p.c[3]
	cub := P{[]float64{p.c[0] / A, p.c[1] / A, p.c[2] / A, 1}}
	p = nil // nil out for safety; not needed

	// Solve related polynomial where D is -1 by
	// replacing x with -cbrt(D)x (and dividing through by -D).
	dd := -cub.c[0]
	cbrtD := math.Cbrt(dd)
	// cub.A *= dd  // cancels out in division
	cub.c[2] /= cbrtD
	cub.c[1] /= cbrtD * cbrtD
	cub.c[0] = -1 // cub.c[0] /= dd

	scale := cbrtD

	// f(0) is -1, and f(x) tends to infinity with x
	// f(1) (is 0, making x=1 a root, or) is either:
	// > 0: meaning there is a root in interval [0 1]; or,
	// < 0: meaning replacing x by 1/x will yield
	// a polynomial with a root in interval [0 1].

	y1 := cub.Eval(1)

	if y1 == 0.0 {
		return scale, nil
	}

	reciprocate := false
	if y1 < 0 {
		// Replace x with 1/x and so on.
		// The cute B,C = -C,-B transformation
		// is only possible because A is 1 and D is -1.
		reciprocate = true
		cub.c[2], cub.c[1] = -cub.c[1], -cub.c[2]
	}

	x := bisect(func(x float64) bool { return cub.Eval(x) >= 0 }, 0, 1)

	if reciprocate {
		x = 1 / x
	}
	x *= scale

	return x, nil
}

// Assuming f is monotonic between l and r
// and f(l) is false (changes from false to true at most once
// between l and r), find the smallest x such that f(x) is true
// and l <= x <= r.
func bisect(f func(x float64) bool, l, r float64) float64 {
	for {
		x := (l + r) / 2
		if x == l || x == r {
			return r
		}
		if f(x) {
			r = x
		} else {
			l = x
		}
	}
}

// Return the real solutions to the quadratic:
// q[0] + q[1]x + q[2]x² == 0
//  c      b       a
func QuadraticSolve(q []float64) ([]float64, error) {
	b := q[1]

	// https://en.wikipedia.org/wiki/Discriminant
	discriminant := DiffOfProducts(b, b, 4.0*q[2], q[0])

	if discriminant < 0 {
		// No real roots, only complex
		return nil, nil
	}
	if discriminant == 0 {
		// repeated root
		r := -b / (2 * q[2])
		return []float64{r, r}, nil
	}

	// 2 real roots
	// https://people.eecs.berkeley.edu/~wkahan/Qdrtcs.pdf
	// https://stackoverflow.com/a/50065711/242457
	// (this follows the stackoverflow answer)

	t := -0.5 * (b + math.Copysign(math.Sqrt(discriminant), b))
	x0 := t / q[2]
	x1 := q[0] / t
	return []float64{x0, x1}, nil
}

// https://stackoverflow.com/a/50065711/242457
/*
  diff_of_products() computes a*b-c*d with a maximum error <= 1.5 ulp

  Claude-Pierre Jeannerod, Nicolas Louvet, and Jean-Michel Muller,
  "Further Analysis of Kahan's Algorithm for the Accurate Computation
  of 2x2 Determinants". Mathematics of Computation, Vol. 82, No. 284,
  Oct. 2013, pp. 2245-2264
*/
func DiffOfProducts(a, b, c, d float64) float64 {
	w := d * c
	e := math.FMA(-d, c, w)
	f := math.FMA(a, b, -w)
	return f + e
}
