package way

import (
	"log"
	"math"
	"sort"
)

import (
	"testing"
)

func TestBez(t *testing.T) {
	b := &B{
		[]Point{{0, 0}, {0, 1}, {1, -1}, {1, 0}}}
	at := 0.25 // hits at ≈ 0.045 and 0.219
	ps, err := b.HorizontalHit(at)
	if err != nil {
		log.Fatal(err)
	}
	CheckHits(t, b, at)
	log.Println(b, at, ps)
}

func CheckHits(t *testing.T, b *B, y float64) {
	ps, err := b.HorizontalHit(y)
	if err != nil {
		log.Fatal(err)
	}
	for i := range ps {
		if math.Abs(y-ps[i].Y) > 0.0001 {
			t.Errorf("Hit on %v at %v, missed [%v] %v",
				b, y, i, ps)
		}
	}

	sorted := sort.SliceIsSorted(ps,
		func(i, j int) bool {
			return ps[i].X < ps[j].X
		})
	if !sorted {
		t.Errorf("Hits on %v at %v, should be sorted in X: %v",
			b, y, ps)
	}
}
