package way

import (
	"log"
	"sort"
)

// Short for Bézier
type B struct {
	P []Point
}

type Point struct {
	X, Y float64
}

// return the [X Y] point for the given parameter value t
func (b *B) Eval(t float64) Point {
	switch len(b.P) {
	case 2:
		return b.Eval1(t)
	}
	x := Bernstein3(t, b.P[0].X, b.P[1].X, b.P[2].X, b.P[3].X)
	y := Bernstein3(t, b.P[0].Y, b.P[1].Y, b.P[2].Y, b.P[3].Y)
	return Point{x, y}
}

func (b *B) Eval1(t float64) Point {
	u := 1 - t
	x := b.P[0].X*u + b.P[1].X*t
	y := b.P[0].Y*u + b.P[1].Y*t
	return Point{x, y}
}

// Blend for the 4 values at the position t, using the
// Bernstein polynomials of degree 3.
func Bernstein3(t float64, A, B, C, D float64) float64 {
	t2 := t * t
	t3 := t2 * t
	u := 1 - t
	u2 := u * u
	u3 := u2 * u

	return A*u3 + B*3*t*u2 + C*3*t2*u + D*t3
}

// For an infinite horizontal ray at the given Y-coordinate,
// return the points at which the ray intersects the Bézier curve.
// The Bézier curve is assumed to be finite, evaluated only
// between 0 <= t <= 1 where t is the interpolation parameter.
// Hence, intersection points will only be returned if they
// correspond to a t-value between 0 and 1.
// The points returned are the result of evaluating the
// Bézier curve at a particular value of t, so
// the y-value will, in general, not be exactly the same as
// the input y.
// The returned points are sorted in order of increasing x
// (and for points with the same x, in order of increasing y).
func (b *B) HorizontalHit(y float64) ([]Point, error) {
	switch len(b.P) {
	case 2:
		return b.horizontalHit1(y)
	}

	// use the y cubic to solve for t (clipped to [0 1])
	// plug that into x, giving a sequence of [x y] points.

	// :todo: implement 1- and 2-order Béziers properly.
	// quick-and-dirty hack to evaluate straight lines.
	if len(b.P) == 2 {
		mx := (b.P[0].X + b.P[1].X) / 2.0
		my := (b.P[0].Y + b.P[1].Y) / 2.0
		b.P = []Point{b.P[0], {mx, my}, {mx, my}, b.P[1]}
	}

	// y = (1-t)**3 P[0] + 3t*(1-t)**2 P[1] +
	//     3t**2*(1-t) P[2] + t**3 P[3]

	A, B, C, D := b.P[0].Y, b.P[1].Y, b.P[2].Y, b.P[3].Y

	// expand the y polynomial out, so we have a single
	// coefficient for each degree of t
	c := make([]float64, 4)
	c[0] = A
	c[1] = -3*A + 3*B
	c[2] = 3*A - 6*B + 3*C
	c[3] = -A + 3*B - 3*C + D
	// Solve for the horizontal ray Y=y
	c[0] -= y

	cub := P{c}
	ts, err := cub.Solve()
	if err != nil {
		return nil, err
	}

	hits := []Point{}
	for _, t := range ts {
		if t < 0 || 1 < t {
			continue
		}
		hits = append(hits, b.Eval(t))
	}

	sortHits(hits)

	return hits, nil
}

// Return the hits when the Bézier curve is a line.
// At most one hit returned.
func (b *B) horizontalHit1(y float64) ([]Point, error) {
	if len(b.P) != 2 {
		log.Fatalf("it is not appropriate to call this method with this Bézier %v", b)
	}
	// y = b.P[0].Y*(1-t) + b.P[1].Y*t
	// y = b.P[0].Y + (b.P[1].Y-b.P[0].Y)*t
	// t = (y - b.P[0].Y) / (b.P[1].Y-b.P[0].Y)

	hits := []Point{}

	if b.P[0].Y == b.P[1].Y {
		// line is exactly horizontal.
		// :todo: what should be done when line hits Y=y?
		return hits, nil
	}

	t := (y - b.P[0].Y) / (b.P[1].Y - b.P[0].Y)
	if t < 0 || 1 < t {
		return hits, nil
	}
	hits = append(hits, b.Eval(t))
	return hits, nil
}

// sort slice (mutates it).
func sortHits(hits []Point) {
	sort.Slice(hits, func(i, j int) bool {
		return hits[i].X < hits[j].X ||
			(hits[i].X == hits[j].X && hits[i].Y < hits[j].Y)
	})
}
